
# threading

This project provides a simple and easy to use, general purpose thread pool.
It can scale to an arbitrary number of threads, in order to conveniently
parallelise jobs. The library allows the completion of jobs to be polled
and waited for. It also provides the ability to control the execution of jobs
using *barrier jobs* and *tokens*.

This library was developed primarily for use within Rasc. However, considering
how useful this library is, I have subsequently used it in many other (smaller)
projects.

The functionality of this project is loosely based on another library which
I wrote in Java a while ago: 
[threadManagement-java](https://gitlab.com/wilsonco-moo/threadManagement-java).

Note that I consider this library to be stable and functionally complete: it
will not receive any updates which make it incompatible with previous versions,
and is unlikely to see any updates which bring major new features. The only
changes this library is ever likely to see from now on are improvements to
documentation and bugfixes.


## Compiling this library, or including it into a project

The header and source files for the library are located in the `src`
directory. These can be copied into another project, included as a git
submodule, or included using any similar build system feature.

This library requires a C++11 compliant compiler, although I have only tested
it with GCC. This project uses the C++ STL, and requires linking to pthread
(`-lpthread`). It does not depend on any other libraries.


## Terminology and features

 * **Job:** This is a unit of work, added to the thread queue as a `std::function`
   (it is convenient to use lambda statements here). Jobs are executed
   asynchronously by one of the thread queue's background threads. Each job
   is either classified as a *normal job* or a *barrier job*.
 * **Normal job:** Many normal jobs are run simultaneously, each executed by one
   of the many threads associated with the thread queue. Normal jobs are
   *started* in the order which they are added to the queue, but since each one
   may take a different amount of time, they may not *finish* in the same order.
 * **Barrier job:** A barrier job is not started until after *all* jobs which
   were added before it have been completed. A barrier job is run by one of the
   threads, and while it is running no other jobs are started. Other jobs are
   not started until *after* the barrier job *completes*. In the diagram below,
   job number 13 is a barrier job.
    - This provides a convenient way to wait for *all* jobs to complete, by
      adding a barrier job and waiting for it to finish.
    - Barrier jobs are also helpful for performing synchronisation actions, which
      cannot be done while another job is running (like assembling results and
      getting ready to run more jobs).
      
   ![Image](images/barrierDiagram.svg)
 * **Token:** When a job is added to the thread queue, a token can be optionally
   supplied (although this is recommended). Later on the token can be deleted,
   which has the following effect:
    - The completion of any jobs which were added using the token are waited
      for.
    - All queued jobs using this token are removed from the queue, and will
      never start.
      
   Doing this has the following benefits:
    - It allows easily cancelling a group of jobs.
    - If running jobs which relate to a class, it is recommended to use a token.
      The token can be created in the constructor of the class, all jobs added
      using it, then deleted in the destructor. This way, it is guaranteed that
      no background job uses the class *after* the class has been deleted.


## Usage examples

The simplest use case is just running some jobs in parallel. To do this, create
a thread queue, add some jobs to it. To wait for all previously added jobs to
complete, add a barrier (an empty barrier job which doesn't do anything),
then wait for it to complete.

The default constructor of ThreadQueue creates the same number of threads as
are available to the hardware (`std::thread::hardware_concurrency()`), in order
to make best use of the hardware.

All started threads are joined and stopped automatically, and any waiting jobs
discarded, when the thread queue is deleted.

```c++
threading::ThreadQueue threadQueue;

for (int i = 0; i < 10; i++) {
    threadQueue.add([](void) {
        // Do something
    });
}

threadQueue.waitForJobToComplete(threadQueue.addBarrier());
```

Barriers can also be used to make sure that a first set of jobs is guaranteed
to complete, before a second set of jobs is started.

In the following example, an alternate constructor is used for the thread queue.
By providing a number (3), it forces the thread queue to always use exactly
three background threads, regardless of the number of threads available to the
hardware.

```c++
threading::ThreadQueue threadQueue(3);

for (int i = 0; i < 10; i++) {
    threadQueue.add([](void) {
        // Do something in first job group (always before barrier)
    });
}

threadQueue.addBarrier([](void) {
    // Do something in the middle (during barrier)
});

for (int i = 0; i < 10; i++) {
    threadQueue.add([](void) {
        // Do something in second job group (always after barrier)
    });
}

threadQueue.waitForJobToComplete(threadQueue.addBarrier());
```

As is shown in the following example, tokens can be used to control the scope
of thread queue jobs. A token can be created in the constructor of a class,
then deleted in its destructor. This ensures that all jobs are stopped before
the class is deleted.

The example below also uses the third constructor of thread queue, where a
minimum and maximum thread count is provided. At least `minimum` threads
will be created, but no more than `maximum`. Otherwise, the thread count is
matched with the number available to the hardware. This allows an appropriate
number of threads to be created, without creating an unnecessarily large number
of threads if run on a system with many available threads.

```c++
class SomeClass {
private:
    threading::ThreadQueue * threadQueue;
    void * token;
    
public:
    SomeClass(threading::ThreadQueue * threadQueue) :
        threadQueue(threadQueue),
        token(threadQueue->createToken()) {
        
        for (int i = 0; i < 100; i++) {
            threadQueue->add(token, [this](void) {
                // Job which relates to parent class. Notice that the token
                // is used when adding it to the queue.
            });
        }
    }
    
    ~SomeClass(void) {
        // After this returns, no jobs added by us will subsequently run.
        threadQueue->deleteToken(token);
    }
};

int main(void) {
    threading::ThreadQueue threadQueue(1, 4);
    {
        SomeClass someClass(&threadQueue);
        std::this_thread::sleep_for(std::chrono::seconds(5));
    }
    std::this_thread::sleep_for(std::chrono::seconds(5));
    return 0;
}
```

For more information and details, see the documentation in `ThreadQueue.h`.
