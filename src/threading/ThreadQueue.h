/*
 * ThreadQueue.h
 *
 *  Created on: 28 Jan 2019
 *      Author: wilson
 */

#ifndef THREADING_THREADQUEUE_H_
#define THREADING_THREADQUEUE_H_

#include <unordered_set>
#include <cstddef>
#include <cstdint>
#include <functional>
#include <list>
#include <unordered_map>

#include "Killable.h"
#include "JobStatus.h"

namespace std {
    class thread;
}



namespace threading {

    /**
     * This returns the name of a job status enum, as a string. This is helpful for debug purposes.
     */
    const char * jobStatusName(JobStatus jobStatus);


    /**
     * ThreadQueue is a class which provides a pool of threads for easy multithreading.
     */
    class ThreadQueue : public Killable {
    private:

        class Job;
        class Token;

        // --------------- Job data structures --------------

        // These maps store the jobs which are waiting, and the jobs which are running, by their job id.
        // All jobs, regardless of type, should be stored in these maps.
        // A job should be removed from waiting and added to running when it is started, and removed from running when it ends.
        std::unordered_map<uint64_t, Job *> waitingJobs,
                                            runningJobs;

        // These data structures store queues of jobs for the thread queue.
        // These lists are used by threads to select the next job to run.
        // Jobs should be removed from these lists when the jobs are started.
        std::list<Job *> normalJobs,
                       * threadSpecificJobs,
                         barrierJobs;

        // ---------------- Stats used internally -----------

        // The number of threads we were created with.
        size_t threadCount;

        // This is set to true to gracefully stop the threads.
        bool endThreads;

        // The number of threads actually running. This is initially set to threadCount,
        // then decremented each time a thread ends. This is used for determining the result of isRunning.
        size_t threadsRunning;

        // This is used for picking a different thread each time getExampleThread is called.
        size_t nextThreadToPick;

        // Stores, for each barrier job, how many threads are waiting for the barrier job to start.
        // A barrier job should only be started when (threadCount-1) threads are waiting for the barrier job.
        // Likewise a thread should wait if fewer than (thredCount-1) threads ware waiting for the barrier job.
        std::unordered_map<uint64_t, size_t> threadCountWaitingForBarrierJobs;

        // --------------------------------------------------

        // This class is used internally to represent a token.
        class Token {
        public:
            // These sets store any waiting or running jobs, which are assigned to this token, regardless of job type.
            // When a job is started it should be removed from waiting, and added to running.
            // When a job ends it should be removed from running.
            std::unordered_set<Job *> waitingJobs,
                                      runningJobs;
            Token(void);
            ~Token(void);
        };

        // This class is used internally to represent a job.
        class Job {
        public:
            // The function we are representing
            std::function<void(void)> func;
            // The time we were added.
            uint64_t timeAdded;

            // The token we are related to, or NULL otherwise.
            Token * token;

            // Our iterator and list, so we can be easily removed.
            std::list<Job *>::iterator iter;
            std::list<Job *> * list;

            Job(std::function<void(void)> func, uint64_t timeAdded, Token * token, std::list<Job *> * list);
            virtual ~Job(void);
        };

        // This condition variable is notified each time a job completes.
        std::condition_variable_any jobWaitCondition;

        // The thread instances we are using, and a pointer to one after the last one (for convenience).
        std::thread ** threads,
                    ** threadsEnd;

        // This is used to give each new job a unique id.
        uint64_t jobIdUpTo;

        // This stores all of the tokens.
        std::unordered_set<Token *> tokens;

        // Don't allow copying, we hold threads and various other raw resources.
        ThreadQueue(const ThreadQueue & other);
        ThreadQueue & operator = (const ThreadQueue & other);

    public:

        /**
         * This should be used for a job id of jobs which are as yet uninitialised, or don't exist.
         * The value of this is the largest value possible to store in a 64 bit integer.
         * This will always have the status of "complete".
         */
        static const uint64_t INVALID_JOB;

        /**
         * Creates a ThreadQueue with the specified number of threads.
         * It does not make any sense for this number to be zero. If this number is zero, no threads will be
         * launched, and the ThreadQueue will do nothing at all.
         */
        ThreadQueue(size_t threadCount);

        /**
         * Creates a ThreadQueue with the default number of threads. By default, this is set to the number of
         * threads available to the hardware, i.e: std::thread::hardware_concurrency().
         *
         * NOTE: This may launch far more than necessary threads, if the program is run on a computer with a large
         *       number of cores/threads. That would waste memory and time. The minimum/maximum constructor should be preferred.
         */
        ThreadQueue(void);

        /**
         * Creates a ThreadQueue where the number of threads launched is between the minimum and the maximum value
         * provided, INCLUSIVELY.
         *
         *  > If the number of threads available to the hardware is less than the provided minimum, minimumThreadCount threads
         *    will be launched.
         *  > If the number of threads available to the hardware is between minimumThreadCount and maximumThreadCount, then
         *    a number of threads equal to what is available to the hardware will be launched.
         *  > If the number of threads available to the hardware is greater than the provided maximum, maximumThreadCount threads
         *    will be launched.
         *
         * NOTE: This constructor should be preferred over the default constructor, as when the program is run on a system
         *       with far more threads available than the program needs, many unnecessary threads will be launched. That would
         *       waste memory and time.
         */
        ThreadQueue(size_t minimumThreadCount, size_t maximumThreadCount);

        virtual ~ThreadQueue(void);

    private:
        // The method used internally for the thread's logic.
        void threadMethod(size_t threadId);

        // ------------------------ internal helper methods ------------------

        // Used for the minimum-maximum thread count constructor.
        // See that constructor for details.
        static size_t getMinMaxThreadCount(size_t minimum, size_t maximum);

        // Returns the next job that ought to be run by this thread.
        // NOTE: This method:
        //   > Can return NULL if there are no jobs that need executing
        //   > Can return a Job if one needs executing
        //   > Can wait (multiple times) if there are barrier jobs, then return either NULL or a Job.
        // The lock should be passed to the method so this method can wait.
        Job * pickJob(size_t threadId, std::unique_lock<std::recursive_mutex> & lock);

        // Updates the internal data structures when a job is added. A new Job is created from the specified token.
        // This adds the job to the *waiting* data structures.
        uint64_t dataStructuresAddJob(std::list<Job *> * list, Token * token, std::function<void(void)> job);

        // This removes the job from the *waiting* data structures. This must be called from within synchronisation.
        void dataStructuresRemoveFromWaiting(Job * job);

        // This adds the job to the *running* data structures. This must be called from within synchronisation.
        void dataStructuresAddToRunning(Job * job);

        // This removes the job from the *running* data structures. This must be called from within synchronisation.
        void dataStructuresRemoveFromRunning(Job * job);

        // Updates the internal data structures when a job is started. This removes the job from *waiting* lists,
        // and adds it to *running* lists.
        // This must be called from within synchronisation.
        inline void dataStructuresOnStartJob(Job * job) {
            dataStructuresRemoveFromWaiting(job);
            dataStructuresAddToRunning(job);
        }

        // Updates the internal data structures when a job is finished. This removes the job from *running* lists,
        // and is simply a wrapper for dataStructuresRemoveFromRunning, to aid readability.
        // This must be called from within synchronisation.
        void dataStructuresOnEndJob(Job * job) {
            dataStructuresRemoveFromRunning(job);
        }

        // ----------------------------------------------------------------------
    public:

        /**
         * Adds a normal job to the thread queue. This job will be started on any thread, when no
         * jobs added before this job are waiting.
         *
         * If a job std::function is not supplied, an empty job which does nothing will be executed.
         * A token to assign to the job can be optionally supplied.
         * The job ID is returned.
         */
        inline uint64_t add(std::function<void(void)> job = [](void){}) { return add(NULL, job); }
        uint64_t add(void * token, std::function<void(void)> job = [](void){});

        /**
         * Adds a job to a specific thread ID. This job will be run by that specific thread, when no
         * jobs added before this job are waiting.
         *
         * A token to assign to the job can be optionally supplied.
         * The job ID is returned.
         */
        inline uint64_t addToThread(size_t threadId, std::function<void(void)> job = [](void){}) { return addToThread(NULL, threadId, job); }
        uint64_t addToThread(void * token, size_t threadId, std::function<void(void)> job = [](void){});

        /**
         * Adds a barrier job to the thread queue. The following three properties are guaranteed about barrier jobs:
         *  > All jobs added before the barrier job will be completed before the barrier job starts.
         *  > No other job will be started during the time that the barrier job is running.
         *  > All jobs added after the barrier job will be started after the barrier job completes.
         *
         * A token to assign to the job can be optionally supplied.
         * The job ID is returned.
         */
        inline uint64_t addBarrier(std::function<void(void)> job = [](void){}) { return addBarrier(NULL, job); }
        uint64_t addBarrier(void * token, std::function<void(void)> job = [](void){});

        /**
         * Creates a token for launching jobs with.
         */
        void * createToken(void);

        /**
         * Deletes the token.
         * This call removes all waiting jobs which use this token, and waits for the completion of any currently
         * running jobs which have this token.
         *
         * NOTE: This should be called once the token is no longer being used, as otherwise memory will be wasted storing
         *       unused job tokens in the data structures within ThreadQueue.
         *
         * The following three properties are guaranteed after this call returns:
         *  > No jobs with the supplied token will be running
         *  > No jobs with the supplied token will be waiting to start
         *  > No job with the supplied token will ever be started from this point onwards, even if it is
         *    added to the thread queue, (adding a job with the supplied token to the thread queue will have no effect).
         */
        void deleteToken(void * token);

        /**
         * This returns the current status of the supplied job, as a JobStatus enum.
         * See the documentation for JobStatus (above) for information about what each state means.
         */
        JobStatus getJobStatus(uint64_t jobId);

        /**
         * Returns the the ID of a thread which exists within this thread queue. Thread IDs returned using this method can
         * be used for the addToThread methods.
         *
         * This method will spread evenly the threads returned in a round-robin approach, to try to keep load on all threads
         * even.
         */
        size_t getExampleThread(void);

        /**
         * Returns the number of threads being used for this thread queue.
         */
        inline size_t getThreadCount(void) {
            return threadCount;
        }

        /**
         * This method waits until the job with the specified id has completed.
         * Once this method returns, the job with the specified id will have completed running and been removed.
         */
        void waitForJobToComplete(uint64_t jobId);

        /**
         * This method waits until the job with the specified id is no longer waiting, and is running.
         * Once this method returns, the job with the specified id has started execution, or may (unlikely) have even already finished.
         */
        void waitForJobToStart(uint64_t jobId);


        // --------- Killable methods, see Killable.h ----------
        virtual void kill(void) override;
        virtual void waitForEnd(void) override;
        virtual bool isRunning(void) override;
        // -----------------------------------------------------
    };

}

#endif
