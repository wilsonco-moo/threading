/*
 * ThreadQueue.cpp
 *
 *  Created on: 28 Jan 2019
 *      Author: wilson
 */

#include "ThreadQueue.h"

#include <condition_variable>
#include <mutex>
#include <system_error>
#include <thread>

// ------------------------------- DEBUG LOGGING MODE -----------------------------------

// Uncomment this to enable debug mode, where a large amount of information is logged to the console.
//#define THREAD_QUEUE_DEBUG_MODE

// Define DEBUG_LOG to print stuff only if THREAD_QUEUE_DEBUG_MODE is enabled.
#ifdef THREAD_QUEUE_DEBUG_MODE
    #include <iostream>
    #define DEBUG_LOG(data) std::cout << data
#else
    #define DEBUG_LOG(data)
#endif

// --------------------------------------------------------------------------------------

// Thread count should default to the total number of threads available to the hardware.
#define DEFAULT_THREAD_COUNT std::thread::hardware_concurrency()

namespace threading {

    const char * jobStatusName(JobStatus jobStatus) {
        switch(jobStatus) {
        case JobStatus::waiting:
            return "waiting";
        case JobStatus::running:
            return "running";
        case JobStatus::complete:
            return "complete";
        default:
            return NULL;
        }
    }

    // ------------------------ Job inner class ----------------------
        ThreadQueue::Job::Job(std::function<void(void)> func, uint64_t timeAdded, Token * token, std::list<Job *> * list) :
            func(func),
            timeAdded(timeAdded),
            token(token),
            iter(),
            list(list) {
        }
        ThreadQueue::Job::~Job(void) {
        }
    //-------------------------- Token inner class -------------------
        ThreadQueue::Token::Token(void) :
            waitingJobs(),
            runningJobs() {
        }
        ThreadQueue::Token::~Token(void) {
        }
    // ---------------------------------------------------------------


    const uint64_t ThreadQueue::INVALID_JOB = (uint64_t)(-1);


    ThreadQueue::ThreadQueue(size_t threadCount) :
        Killable(),
        waitingJobs(),
        runningJobs(),
        normalJobs(),
        threadSpecificJobs(new std::list<Job *>[threadCount]),
        barrierJobs(),
        threadCount(threadCount),
        endThreads(false),
        threadsRunning(threadCount),
        nextThreadToPick(0),
        threadCountWaitingForBarrierJobs(),
        jobWaitCondition(),
        threads(new std::thread * [threadCount]),
        threadsEnd(threads + threadCount),
        jobIdUpTo(0),
        tokens() {

        DEBUG_LOG("Creating ThreadQueue with an initial thread count of " << threadCount << "\n");

        // Create each thread, don't detach them: We want to join them later.
        for (size_t i = 0; i < threadCount; i++) {
            threads[i] = new std::thread(&ThreadQueue::threadMethod, this, i);
        }
    }

    ThreadQueue::ThreadQueue(void) :
        ThreadQueue(DEFAULT_THREAD_COUNT) {
    }

    ThreadQueue::ThreadQueue(size_t minimumThreadCount, size_t maximumThreadCount) :
        ThreadQueue(getMinMaxThreadCount(minimumThreadCount, maximumThreadCount)) {
    }

    ThreadQueue::~ThreadQueue(void) {
        // This will kill all threads then wait for them to end, so we can now safely delete
        // all the dynamically allocated resources that we
        killableDestroy();

        // Delete each thread, then the array we were storing them in.
        for (std::thread ** thread = threads; thread != threadsEnd; thread++) {
            DEBUG_LOG("Deleting thread\n");
            delete *thread;
        }
        delete[] threads;

        // Delete all tokens
        for (Token * token : tokens) {
            delete token;
        }

        // Delete all jobs which are still waiting. We don't need to delete from runningJobs, there
        // cannot be any at this point.
        for (std::pair<uint64_t, Job *> jobPair : waitingJobs) {
            delete jobPair.second;
        }

        // Delete thread specific job data structure as well, since this is dynamically allocated.
        delete[] threadSpecificJobs;
    }




    void ThreadQueue::threadMethod(size_t threadId) {

        while(true) {
            Job * job;
            // Get a job using pickJob. If pickJob returns NULL, wait and repeat.
            // Before, between and after each time we wait, we MUST check if we should end.
            // Note that pickJob sometimes waits, and sometimes does not wait, but either way *can* return NULL, or a Job.
            {
                std::unique_lock<std::recursive_mutex> lock(killableMutex);
                if (endThreads) goto threadMethodEnd;
                while ((job = pickJob(threadId, lock)) == NULL) {
                    if (endThreads) goto threadMethodEnd;
                    DEBUG_LOG("Thread " << threadId << ": No jobs returned by pickJob, waiting.\n");
                    killableCondition.wait(lock);
                    if (endThreads) goto threadMethodEnd;
                }
                if (endThreads) goto threadMethodEnd;
                dataStructuresOnStartJob(job);
            }
            // Run the job.
            job->func();
            // Finally, delete the job, and notify the condition for waiting for jobs to finish.
            {
                std::lock_guard<std::recursive_mutex> lock(killableMutex);
                dataStructuresOnEndJob(job);
            }
            delete job;
            jobWaitCondition.notify_all();
        }

        threadMethodEnd:;

        // At the end, decrement the number of threads running.
        std::lock_guard<std::recursive_mutex> lock(killableMutex);
        threadsRunning--;
        DEBUG_LOG("Thread " << threadId << " ended.\n");
    }




    // ------------------------------------------------ internal helper methods -------------------------------------------


    size_t ThreadQueue::getMinMaxThreadCount(size_t minimum, size_t maximum) {
        size_t hardwareCount = std::thread::hardware_concurrency();
        if (hardwareCount < minimum) {
            return minimum;
        } else if (hardwareCount > maximum) {
            return maximum;
        } else {
            return hardwareCount;
        }
    }


    /**
     * This macro waits for the specified job id to complete, before returning.
     * It is assumed that the lock is an already locked unique_lock.
     * This is a macro because both pickJob and waitForJobToComplete need to do this.
     *
     * An extra condition is supplied, so the user can for example specify that we only wait while
     * it has not been requested to end threads.
     */
    #define WAIT_FOR_JOB_TO_COMPLETE(jobId, lock, extraCondition)              \
        /* First wait until this job is no longer waiting.                   */\
        while(waitingJobs.find(jobId) != waitingJobs.end() extraCondition) {   \
            jobWaitCondition.wait(lock);                                       \
        }                                                                      \
        /* Next wait until this job is no longer running.                    */\
        while(runningJobs.find(jobId) != runningJobs.end() extraCondition) {   \
            jobWaitCondition.wait(lock);                                       \
        }


    // (For pseudocode for how jobs are picked, see the bottom of this file).

    ThreadQueue::Job * ThreadQueue::pickJob(size_t threadId, std::unique_lock<std::recursive_mutex> & lock) {

        // Loop while there are barrier jobs waiting...
        while (!barrierJobs.empty()) {
            DEBUG_LOG("--------- thread " << threadId << ": There exists a barrier job ...\n");
            Job * barrierJob = barrierJobs.front();
            uint64_t barrierJobTimeAdded = barrierJob->timeAdded;

            // If neither normalJobs nor threadSpecificJobs has a job older than this barrier job:
            if ( (normalJobs.empty() || normalJobs.front()->timeAdded > barrierJobTimeAdded) &&
                 (threadSpecificJobs[threadId].empty() || threadSpecificJobs[threadId].front()->timeAdded > barrierJobTimeAdded)) {
                DEBUG_LOG("--------- thread " << threadId << ": ... and it is the oldest job ...\n");
                // If it is not the case that all other threads are waiting for THIS barrier job:
                if (threadCountWaitingForBarrierJobs[barrierJobTimeAdded] < threadCount - 1) {
                    DEBUG_LOG("--------- thread " << threadId << ": But not all other threads are waiting for THIS barrier job, so waiting.\n");
                    // Then wait for this barrier job to complete, and loop again in case another barrier job is waiting immediately.
                    // We don't check endThreads before waiting, since we start in a mutex we can assume that endThreads has already been checked.

                    // Increment the number of threads waiting for THIS barrier job.
                    threadCountWaitingForBarrierJobs[barrierJobTimeAdded]++;

                    // Wait for the job to complete, but only while it has not been requested that we end threads.
                    WAIT_FOR_JOB_TO_COMPLETE(barrierJobTimeAdded, lock, && !endThreads)

                    // Decrement the number of threads waiting for THIS barrier job, and remove the entry if it reaches zero.
                    threadCountWaitingForBarrierJobs[barrierJobTimeAdded]--;
                    if (threadCountWaitingForBarrierJobs[barrierJobTimeAdded] == 0) {
                        threadCountWaitingForBarrierJobs.erase(barrierJobTimeAdded);
                        DEBUG_LOG("--------- thread " << threadId << ": No-one is now waiting for this barrier job, so removing it's entry.\n");
                    }

                    DEBUG_LOG("--------- thread " << threadId << ": The barrier job has finished, checking again if there is another barrier job.\n");
                    // Since we just waited, we need to check endThreads again. If endThreads is true, immediately return.
                    if (endThreads) return NULL;

                } else {
                    DEBUG_LOG("--------- thread " << threadId << ": ... everyone else seems to be waiting for THIS barrier job, so we should execute it.\n");
                    // If all other threads are waiting, then we should execute this barrier job.
                    return barrierJob;
                }
            } else {
                DEBUG_LOG("--------- thread " << threadId << " ... but there is a newer job elsewhere that we can run.\n");
                // If there is a newer job somewhere, then we can just execute that.
                break;
            }
        }

        // If there are no barrier jobs that we can execute:

        // If there are no thread specific jobs to execute:
        if (threadSpecificJobs[threadId].empty()) {

            // If there are no normal jobs to execute:
            if (normalJobs.empty()) {
                // Then we cannot to anything.
                DEBUG_LOG("--------- thread " << threadId << ": No other jobs found to execute.\n");
                return NULL;

            // If there is a normal job to execute:
            } else {
                // Then do it.
                DEBUG_LOG("--------- thread " << threadId << ": Launching a normal job, only a normal job is currently available.\n");
                return normalJobs.front();
            }

        // If there is a thread specific job we can execute:
        } else {

            // If there are no normal jobs to execute:
            if (normalJobs.empty()) {
                // Then do the thread specific job.
                DEBUG_LOG("--------- thread " << threadId << ": Launching a thread specific job, only a thread specific job is available.\n");
                return threadSpecificJobs[threadId].front();

            // If there is also a normal job to execute:
            } else {
                DEBUG_LOG("--------- thread " << threadId << ": Both normal jobs and thread specific jobs are available, picking the oldest.\n");
                // Then pick whichever one was added first.
                Job * normal         = normalJobs.front(),
                    * threadSpecific = threadSpecificJobs[threadId].front();

                return normal->timeAdded < threadSpecific->timeAdded ? normal : threadSpecific;
            }
        }
    }

    uint64_t ThreadQueue::dataStructuresAddJob(std::list<Job *> * list, Token * token, std::function<void(void)> func) {
        uint64_t jobId;
        {
            // Lock to the mutex, this must be an atomic operation.
            std::lock_guard<std::recursive_mutex> lock(killableMutex);
            // Get and increment the job id.
            jobId = jobIdUpTo++;

            // If the token does not exist, and isn't NULL, don't add the job (do nothing).
            if (token != NULL && tokens.find(token) == tokens.end()) {
                return jobId;
            }

            // Create a new job instance, add it to the list.
            Job * job = new Job(func, jobId, token, list);
            list->push_back(job);
            // Set the job's iterator
            job->iter = --list->end();
            // Add the job to the token's jobs set, if the token exists.
            if (token != NULL) {
                token->waitingJobs.insert(job);
            }
            // Add the job to the waiting jobs map.
            waitingJobs[jobId] = job;
        }
        // Notify the threads that a new job has been added.
        killableCondition.notify_all();
        // Return the new job id that we used.
        return jobId;
    }

    void ThreadQueue::dataStructuresRemoveFromWaiting(Job * job) {
        // Remove the job from the queue that it is in.
        // This will be either normalJobs, one of the lists in threadSpecificJobs, or barrierJobs.
        job->list->erase(job->iter);
        // Remove the job from the global waitingJobs map.
        waitingJobs.erase(job->timeAdded);
        // Remove the job from its token's waitingJobs set, (if the job is assigned to a token).
        Token * token = job->token;
        if (token != NULL) {
            token->waitingJobs.erase(job);
        }
    }

    void ThreadQueue::dataStructuresAddToRunning(Job * job) {
        // Add the job to the global runningJobs map.
        runningJobs[job->timeAdded] = job;
        // Add the job to its token's runningJobs set, if the job has a token.
        Token * token = job->token;
        if (token != NULL) {
            token->runningJobs.insert(job);
        }
    }

    void ThreadQueue::dataStructuresRemoveFromRunning(Job * job) {
        // Remove the job from the global runningJobs map.
        runningJobs.erase(job->timeAdded);
        // Remove the job from its token's runningJobs set, if the job has a token.
        Token * token = job->token;
        if (token != NULL) {
            token->runningJobs.erase(job);
        }
    }




    uint64_t ThreadQueue::add(void * token, std::function<void(void)> job) {
        return dataStructuresAddJob(&normalJobs, (Token *)token, job);
    }
    uint64_t ThreadQueue::addToThread(void * token, size_t threadId, std::function<void(void)> job) {
        return dataStructuresAddJob(threadSpecificJobs + threadId, (Token *)token, job);
    }
    uint64_t ThreadQueue::addBarrier(void * token, std::function<void(void)> job) {
        return dataStructuresAddJob(&barrierJobs, (Token *)token, job);
    }



    void * ThreadQueue::createToken(void) {
        std::lock_guard<std::recursive_mutex> lock(killableMutex);
        Token * token = new Token();
        tokens.insert(token);
        return (void *)token;
    }

    // NOTE: THIS MUST NOT BE CALLED FROM WITHIN THE KILLABLE MUTEX.
    void ThreadQueue::deleteToken(void * voidToken) {
        std::unique_lock<std::recursive_mutex> lock(killableMutex);

        Token * token = (Token *)voidToken;
        // Remove the token from the tokens set, so that no new jobs can be added on this token.
        std::unordered_set<Token *>::iterator iter = tokens.find(token);
        if (iter == tokens.end()) {
            // If the token has already been removed, we have nothing to do.
            return;
        } else {
            tokens.erase(iter);
        }

        // First delete all waiting jobs owned by this token.
        // Loop until we have delete all of the token's waiting jobs.
        while(!token->waitingJobs.empty()) {
            // Get a job from the token's jobs set.
            Job * job = *token->waitingJobs.begin();
            // Remove it from the waiting data structures.
            dataStructuresRemoveFromWaiting(job);
            // Delete the job.
            delete job;
        }

        // Next wait for all jobs assigned to this token to complete. Here, simply wait until the
        // token's waitingOrRunningJobs set becomes empty.
        while(!token->runningJobs.empty()) {
            jobWaitCondition.wait(lock);
        }
        // Now there are no running or waiting jobs using this token, we can delete the token and return.
        delete token;
    }

    JobStatus ThreadQueue::getJobStatus(uint64_t jobId) {
        std::lock_guard<std::recursive_mutex> lock(killableMutex);
        if (waitingJobs.find(jobId) != waitingJobs.end()) {
            return JobStatus::waiting;
        } else if (runningJobs.find(jobId) != runningJobs.end()) {
            return JobStatus::running;
        } else {
            return JobStatus::complete;
        }
    }

    size_t ThreadQueue::getExampleThread(void) {
        std::lock_guard<std::recursive_mutex> lock(killableMutex);
        size_t id = nextThreadToPick++;
        if (nextThreadToPick >= threadCount) nextThreadToPick = 0;
        return id;
    }

    // NOTE: THIS MUST NOT BE CALLED FROM WITHIN THE KILLABLE MUTEX.
    void ThreadQueue::waitForJobToComplete(uint64_t jobId) {
        std::unique_lock<std::recursive_mutex> lock(killableMutex);
        WAIT_FOR_JOB_TO_COMPLETE(jobId, lock, )
    }

    void ThreadQueue::waitForJobToStart(uint64_t jobId) {
        std::unique_lock<std::recursive_mutex> lock(killableMutex);
        while(waitingJobs.find(jobId) != waitingJobs.end()) {
            jobWaitCondition.wait(lock);
        }
    }

    void ThreadQueue::kill(void) {
        // The kill method is only supposed to "cause the Killable instance to stop running it's threads within a reasonable amount of time",
        // so all we must do here is tell the threads to end, then notify them.
        {
            std::lock_guard<std::recursive_mutex> lock(killableMutex);
            endThreads = true;
        }
        killableCondition.notify_all();
        jobWaitCondition.notify_all(); // Also notify this in case there are any threads waiting for a barrier.
    }

    void ThreadQueue::waitForEnd(void) {
        // Loop through all threads, try to join each one.
        // This will wait until all of the threads have ended.
        // Any synchronisation here would cause a deadlock. None is needed as the threads are persistent
        // until the destructor body is run, (which cannot happen until after this call terminates anyway).
        for (std::thread ** thread = threads; thread != threadsEnd; thread++) {
            try {
                (*thread)->join();
            } catch (const std::system_error& e) {
                // Ignore errors which are thrown if the thread has already finished.
            }
        }
    }

    bool ThreadQueue::isRunning(void) {
        // Get the current value of threads running. We must access this from within the mutex.
        size_t currentThreadsRunning;
        {
            std::lock_guard<std::recursive_mutex> lock(killableMutex);
            currentThreadsRunning = threadsRunning;
        }
        // Perform the next step outside the mutex, as the next step *could* involve some waiting.
        if (currentThreadsRunning == 0) {
            // If there are no threads running, return false.
            // Since if we return false here we must be completely sure that all threads have definitely ended by this point,
            // call waitForEnd() before returning. This gets around the race condition that could happen if this method is run between
            // the last thread updating the threadsRunning variable, and the operating system actually freeing all of the resources
            // associated with the running of that thread. That would cause us to return false before the thread has *actually* ended.
            waitForEnd();
            return false;
        } else {
            // If there is at least one thread still running, we must still be running.
            // Note that this may happen before the threads have actually started, if this is called very early on. True should still
            // be returned, since threads are in the process of being started.
            return true;
        }
    }
}



/*

Pseudocode for picking jobs


pickJob() {

    while(a barrier job exists) {

        if (the barrier job is the oldest of the current selection) {

            if (threads waiting for barrier job < number of threads - 1) {
                wait until the barrier job has completed, a different thread will execute it in the meantime.
            } else {
                return the barrier job so we can execute it.
            }

        } else {
            break loop, since we should not think about this barrier job yet.
        }

    }

    return oldest between thread specific job and normal job.
}


*/
