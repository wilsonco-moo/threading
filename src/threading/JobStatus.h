/*
 * JobStatus.h
 *
 *  Created on: 4 Feb 2019
 *      Author: wilson
 */

#ifndef THREADING_JOBSTATUS_H_
#define THREADING_JOBSTATUS_H_

namespace threading {

    /**
     * This enum represents the status of a job, as returned by the getJobStatus method.
     */
    enum class JobStatus {
        // This represents that the job is currently waiting to start, and should start in the future.
        waiting,
        // This represents that the job is currently running, and should end in the future.
        running,
        // This represents that the supplied job ID has either:
        //  > Completed running.
        //  > Never been added to the queue in the first place.
        complete
    };

}

#endif
