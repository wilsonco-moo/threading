
/**
 * Killable.h
 *
 * C++ header
 *
 * This header provides the Killable class. The Killable class is a simple class, that provides a
 * recursive_mutex, a condition_variable_any, and a few pure virtual methods.
 *
 * The objective of the Killable class is to define a standard set of behaviour for classes which contain
 * threads or other operating system resources. This behaviour ensures that any class correctly
 * implementing Killable can cleanly exit, and be deleted, in a thread-safe way.
 *
 * In addition to the original Java Killable implementation, it should be thread-safe to destroy a
 * Killable instance at any point in time. This is because the destructor of a correctly implemented
 * Killable class should call killableDestroy() as it's first statement. This method ensures that
 * kill() then waitForEnd() are automatically called before the object is deleted.
 *
 *
 * KILLABLE SUBCLASSES MUST DO THESE THINGS, (suitable synchronised to killableMutex)
 * ----------------------------------------------------------------------------------
 *
 * > Implement the kill method, which should cause the instance to end it's thread at some point within
 *   a reasonable timescale.
 * > Implement the waitForEnd() method, which should either do nothing if the thread is not running, or block
 *   and return only when the thread has ended. After this method returns, it should be safe for the
 *   object to be destroyed.
 * > Implement the isRunning method, which should return whether the thread is actually running.
 * > Call killableDestroy() in the destructor, as the first statement.
 * > Call the super-classes' kill method within our implementation of kill(), if we are not a direct subclass of Killable.
 * > Call the super-classes' waitForEnd method within our implementation of waitForEnd(), if we are not a direct subclass of Killable.
 * > Call the kill() and waitForEnd() methods of any contained Killable objects when our respective methods are called.
 *
 *
 *
 * Users of Killable instances can also use the kill and waitForEnd methods to control the object.
 * The object can call kill() from within itself, causing any waitForEnd call from outside to eventually end.
 */

#ifndef THREADING_KILLABLE_H_
#define THREADING_KILLABLE_H_

#include <mutex>
#include <condition_variable>

namespace threading {
    
    /**
     * The Killable class, described in the above documentation.
     *
     * @author wilson
     */
    class Killable {
    
    protected:
        /**
         * This mutex is provided so that calls to kill, waitForEnd and isRunning can be synchronised.
         */
        std::recursive_mutex killableMutex;

        /**
         * This is a condition variable to be used alongside killableMutex.
         */
        std::condition_variable_any killableCondition;

    public:
        Killable(void);
        virtual ~Killable(void);
        
        /**
         * This can be manually called by users of this class, but is automatically called when the object is destroyed.
         *
         * Calling this must cause the Killable instance to stop running it's threads within a reasonable amount of time.
         * It is not required for the threads to stop executing BEFORE this call returns, but any threads MUST stop executing
         * at some reasonable point in the future during or after this call is run.
         * 
         * At some reasonable time in the future, waitForEnd() calls should return immediately, and isRunning() should return false.
         *
         * IF WE ARE NOT A DIRECT SUBCLASS OF Killable, THIS SHOULD CALL THE SUPERCLASES' kill METHOD ALSO.
         */
        virtual void kill(void) = 0;

        /**
         * This can be manually called by users of this class, but is automatically called when the object is destroyed.
         *
         * This call should block until the Killable instance has stopped executing any of it's threads. This call must only return
         * AFTER all threads are no longer running. If there were no threads running in the first place, this call should do nothing.
         * After this call has returned, it MUST be safe to delete the Killable object.
         * This call should be akin to joining any running threads.
         * 
         * NOTE: The thread join call or wait call should NOT be synchronised on the killable mutex, as otherwise a deadlock would occur
         * if the thread locks to the killable mutex also.
         *
         * Also, when this call returns, the Killable object must be in a state such that isRunning() returns false.
         *
         * IF WE ARE NOT A DIRECT SUBCLASS OF Killable, THIS SHOULD CALL THE SUPERCLASES' waitForEnd METHOD ALSO.
         */
        virtual void waitForEnd(void) = 0;

        /**
         * This should return true if there are any threads running from this object, false otherwise.
         */
        virtual bool isRunning(void) = 0;

    private:
        bool killableNotYetDestroyed;
    protected:
        /**
         * This must be called as the first statement of the destructor of any subclass.
         * This automatically kills and waits for the thread to end, but only does this for the first class that
         * called it. This ensures that a Killable object can be destroyed at any point in time.
         */
        void killableDestroy(void);
    };
}
#endif
