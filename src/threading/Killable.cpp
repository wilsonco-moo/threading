
#include "Killable.h"

namespace threading {

    Killable::Killable(void) :
        killableMutex(),
        killableNotYetDestroyed(true) {
    }
    Killable::~Killable(void) {}

    void Killable::killableDestroy(void) {
        if (killableNotYetDestroyed) {
            kill();
            waitForEnd();
            killableNotYetDestroyed = false;
        }
    }

}
